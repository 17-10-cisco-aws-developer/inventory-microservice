package com.classpath.inventory.model;

public enum Event {
	
	ORDER_ACCEPTED,
	ORDER_PENDING,
	ORDER_FULFILLED,
	ORDER_CANCELLED

}
